﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MovingEntity {

    public GameObject bulletPrefab;

    public float shootCooldown = 0.25f;
    float lastShootTime = 0;

	// Update is called once per frame
	void Update () {
        Vector3 desiredDirection = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            desiredDirection += Vector3.left;
        if (Input.GetKey(KeyCode.D))
            desiredDirection += Vector3.right;
        if (Input.GetKey(KeyCode.W))
            desiredDirection += Vector3.forward;
        if (Input.GetKey(KeyCode.S))
            desiredDirection += Vector3.back;

        MovePlayer(desiredDirection.normalized);

        if( Input.GetButton("Fire1") & (Time.time - lastShootTime > shootCooldown))
            Shoot();

    }

    void Shoot(){
        GameObject.Instantiate(bulletPrefab, transform.position, transform.rotation);
        lastShootTime = Time.time;
    }

}
