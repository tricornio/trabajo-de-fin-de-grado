﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MovingEntity {

    public float areaVagabundeo   = 5f;
    Vector3 posicionObjetivo;
    Vector3 distanciaObjetivo;

    // Calcular una posicion a la que moverse, para que el zombie se mueva por ahi aleatoriamente
    void RecalculateTargetPosition() {
        posicionObjetivo = transform.position + Random.insideUnitSphere * areaVagabundeo;
        posicionObjetivo.y = 0; // para no volar
    }

	// Use this for initialization
	void Start () {
        RecalculateTargetPosition();
	}
	
	// Update is called once per frame
	void Update () {
        //recalculas distancia hacia el punto objetivo
        distanciaObjetivo = posicionObjetivo - transform.position;
        MoveZombie( distanciaObjetivo.normalized );

        // si ya llegaste al objetivo recalculamos un nuevo objetivo
        if (distanciaObjetivo.magnitude <= 0.5f) 
            RecalculateTargetPosition();

        Debug.DrawLine(transform.position, posicionObjetivo, Color.blue);
	}
}
