﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour {

    public GameObject zombiePrefab;
    public float spawnTime = 5;
    public float spawnRadius = 5;

    float lastZombieTime = 0;

    void GenerateZombie() {
        Vector3 randomPosition = transform.position + Random.insideUnitSphere * spawnRadius;
        randomPosition.y = -1;
        GameObject.Instantiate(zombiePrefab, randomPosition, Quaternion.identity);
    }

	// Update is called once per frame
	void Update () {
        if (Time.time - lastZombieTime > spawnTime) {
            GenerateZombie();
            lastZombieTime = Time.time;
        }
	}

}
