﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEntity : MonoBehaviour {

    public float movementSpeed = 1f;
    public float rotationSpeed = 1f;

    protected void MoveZombie (Vector3 direction) {
        Quaternion rotarHaciaObjetivo = Quaternion.LookRotation(direction, Vector3.up);
        // rotamos desde la posicion actual, hacia la objetivo, a X velocidad
        transform.rotation = Quaternion.Lerp(transform.rotation, rotarHaciaObjetivo, rotationSpeed * Time.deltaTime);
        // luego nos movemos (aunque no entiendo muy bien como???)
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }

    protected void MovePlayer(Vector3 direction){
        /*Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);
        transform.rotation = Quaternion.Euler(new Vector3(0f, -90-angle, 0f));*/

        Quaternion rotarHaciaObjetivo = Quaternion.LookRotation(Camera.main.ScreenToWorldPoint(new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            -Camera.main.transform.position.z + 0)) - direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotarHaciaObjetivo, rotationSpeed * Time.deltaTime);
        transform.position += direction * movementSpeed * Time.deltaTime;
    }

    private float AngleBetweenTwoPoints(Vector3 a, Vector3 b){
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

}
