﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public int speed = 100;
    public float flyTime = 1;

    Rigidbody rb;

    void Awake() {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * speed, ForceMode.VelocityChange);
        Invoke("DestroyBullet", flyTime);
    }

    void DestroyBullet() {
        Destroy(gameObject);
    }

    void OnTriggerEnter( Collider other ) {
        if (other.CompareTag("Enemy")) ;
    }

}
